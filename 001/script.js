//define element 
const btn = document.querySelector('#changeBackgroundColor');
const body = document.body;
const arrayColor = ['blue','purple','orange','pink','black','green'];


btn.addEventListener('click', changeBackground);
body.style.backgroundColor = 'orange';

function changeBackground(){
    let arrIndex = parseInt(Math.random()*arrayColor.length);
    body.style.backgroundColor = arrayColor[arrIndex];
} 