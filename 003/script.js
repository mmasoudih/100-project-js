//define variable

const title = document.querySelector('.text');
const author = document.querySelector('.author');
const btn = document.querySelector('.change');

const qoutesArr = [
    {
        text:"رویا چیزی است که همیشه به شما زندگی می دهد. اگر در پی رویاهایتان باشید اگر همین امروز بمیرید انگار سال ها زندگی کرده اید.",
        author:"جیمز دین" 
    },
    {
        text:"مهم نیست کجا ایستاده اید. رویاها و آرزوهایتان همیشه معتبر و مهم هستند. ",
        author:"لوپیتا نیونگو" 
    },
    {
        text:"تاکنون بارها تلاش کرده اید و مدام شکست خورده اید؟ اشکالی ندارد. دوباره تلاش کنید و شکست بخورید این خود از دست روی دست گذاشتن بهتر است.",
        author:"ساموئل بکت" 
    },
    {
        text:"یک مرد نمی میرد مگر اینکه به جای آرزو کردن حسرت را در خود پرورش دهد. ",
        author:"جان بریمور" 
    },
    {
        text:"تنها چیزی که شما را از رسیدن به رویاها و آرزوهایتان باز می دارد خود شما هستید. ",
        author:"تام برادلی" 
    }
];
document.body.addEventListener('DOMContentLoaded', changeQuotes());
btn.addEventListener('click' , changeQuotes);

function changeQuotes(){
    let random = Math.floor(Math.random() * qoutesArr.length);
    title.textContent = qoutesArr[random].text;
    author.textContent = qoutesArr[random].author;
}
