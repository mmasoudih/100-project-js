//define element 
const btn = document.querySelector('#changeBackgroundColor');
const body = document.body;
const arrayColor = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F'];
const value = document.querySelector('#value');

btn.addEventListener('click', changeHex);
body.style.backgroundColor = 'orange';

function changeHex() {
    let hex = '#'
    for (let i = 0; i < 6; i++) {
        const index = Math.floor(Math.random() * arrayColor.length)
        hex += arrayColor[index]
    }
    value.textContent = hex
    body.style.backgroundColor = hex
}